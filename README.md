# API signin/signup - Concrete

Esta API foi desenvolvida atendendo as demandas do desafio, encontradas neste [documento](CHALLENGE.md).

---

# TECH STACK

* Node.js [9.5]
* MongoDB [3.6]
* Docker [17.09]

---

# SETUP

## 1. Docker
Certifique-se de possuir o Docker instalando, junto com o docker-compose.
Para isso, execute os comandos abaixo:
```sh
$ docker -v
Docker version 17.09.1-ce, build 19e2cf6
```
```sh
$ docker-compose -v
docker-compose version 1.15.0, build e12f3b9
```
Caso necessite instala-los, siga a [documentação oficial](https://docs.docker.com/install/).

## 2. Aplicação

Faça o clone do código para um diretório de sua preferência e execute o comando abaixo para levantar as aplicações.

```sh
$ cd [PATH_ONDE_FOI_FEITO_O_DOWNLOAD]
$ docker-compose up
```

Serão levantados 2 containeres, um para a API com o nome **cs-api** e outra para o DB com o nome **cs-db**.


> **ATENÇÃO** **ATENÇÃO** **ATENÇÃO** **ATENÇÃO**


A aplicação já possui algumas variáveis de ambiente configuradas no arquivo **docker-compose.yml**, como por exemplo portas do MongoDB e do Node.js, nível de log e outros.
 
Também existe um arquivo chamado **docker-compose.yml.sample** caso necessite restaurar às configurações iniciais.

---

# ENDPOINTS
Todas as rotas aceitam apenas requisições no formato **.json**.

## GET /api/user/[ID]

Rota para obter um usuário cadastrado.

Substitua o **[ID]** por um id de usuário válido.

É obrigatório possuir um token de autenticação ([documentação](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication))


## POST /api/user/signin

Rota para fazer o login.

Campos obrigatórios:

* email
* senha

Exemplo: 
```json
{
	"email" : "something@domain.com",
	"senha" : "my_secret"
}
```

## POST /api/user/signup

Campos obrigatórios:

* email
* senha
* senha
* telefone, com area e numero

Exemplo: 
```json
{
	"nome" : "John Doe",
	"email" : "something@domain.com",
	"senha" : "my_secret",
	"telefone" : {
		"area" : 12,
		"numero" : 123456789
	}
}
```
