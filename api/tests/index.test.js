const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../index');

let should = chai.should;

Object.keys(mongoose.connection.models).forEach(key => {
    delete mongoose.connection.models[key];
});

chai.use(chaiHttp);

describe('Calling not implemented routes', () => {

    describe('GET /unknown_route', function () {

        it('Should receive MethodNotImplementedError(501) ', (done) => {
            chai.request(app.listen())
                .get('/unknown_route')
                .end((error, response) => {
                    response.should.have.status(501);
                    done();
                });
        });

    });

});