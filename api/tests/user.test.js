const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const sinon = require('sinon');

const app = require('../index');
const authentication = require('../services/authentication');

let should = chai.should;
let expect = chai.expect;

let User = require('../models/user');
let Errors = require('../services/handle_errors');

let userMock = [{
    id: '123',
    name: 'John Doe',
    email: 'johndoe@domain.com',
    password: 'john_secret',
    phone_area: '11',
    phone_number: '12341234',
    token: 'a'
}];

Object.keys(mongoose.connection.models).forEach(key => {
    delete mongoose.connection.models[key];
});

chai.use(chaiHttp);

describe('Calling implemented routes', () => {

    describe('GET /api/user/:id', function () {

        it('Should receive UnauthorizedError(401)', (done) => {
            chai.request(app.listen())
                .get('/api/user/123456789')
                .end((error, response) => {
                    response.should.have.status(401);
                    done();
                });
        });

        it('Should receive UserNotFoundError(404)', (done) => {

            chai.request(app.listen())
                .get('/api/user/123456789')
                .set('authorization', 'Bearer papai')
                .end((error, response) => {
                    response.should.have.status(404);
                    done();
                });
        });

        it('Should receive UnauthorizedError(401), tokens not matching', (done) => {
            done();
        });

        it('Should receive SessionExpiredError(401), session expired', (done) => {
            done();
        });

        it('Should receive the user data(200)', (done) => {
            done();
        });

    });

    describe('POST /api/user/signup', function () {

        it('Should receive BadRequestError(400), fields missing', (done) => {
            chai.request(app.listen())
                .post('/api/user/signup')
                .send({
                    'nome': 'Mary Jane',
                    'email': 'anything@domain.com',
                })
                .end((error, response) => {
                    response.should.have.status(400);
                    done();
                });
        });

        it('Should receive BadRequestError(400), email already exists', (done) => {
            done();
        });

        it('Should receive success(200)', (done) => {
            done();
        });        

    });

    describe('POST /api/user/signin', function () {

        it('Should receive BadRequestError(400), fields missing', (done) => {
            chai.request(app.listen())
                .post('/api/user/signup')
                .send({
                    'senha': 'my_secret'
                })
                .end((error, response) => {
                    response.should.have.status(400);
                    done();
                });
        });

        it('Should receive InvalidCredentialsError(401), email not found', (done) => {
            done();
        });

        it('Should receive InvalidCredentialsError(401), password not matching', (done) => {
            done();
        });        

        it('Should receive success(200)', (done) => {
            done();
        });

    });

});