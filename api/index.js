const SERVER_PORT = process.env.PORT || 3000;
const LOG_LEVEL = process.env.LOG_LEVEL || 'debug';

const express = require('express');
const winston = require('winston');
const bodyParser = require('body-parser');

const { handle } = require('./services/handle_response');
const db = require('./services/db');
const app = express();

winston.level = LOG_LEVEL;

db.init();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const userRoutes = require('./routes/user');
app.use('/api', userRoutes);
app.get('*', (req, res) => handle(res, 501, 'Method not implemented'));

if (!module.parent) {
    app.listen(SERVER_PORT, () => {
        winston.info(`Listening on ${SERVER_PORT}`);
    });
}

module.exports = app;