function handle(response, statusCode, message) {
    response.status(statusCode);
    response.json({'mensagem' : message});
    response.end();     
}

module.exports = { handle };