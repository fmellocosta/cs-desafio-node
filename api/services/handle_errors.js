class BadRequestError extends Error {
    constructor(message) {
        super(message || 'All fields are required');
        this.code = 400;
    }
}

class UserNotFoundError extends Error {
    constructor(message) {
        super(message || 'User not found');
        this.code = 404;
    }
}

class UnauthorizedError extends Error {
    constructor(message) {
        super(message || 'Not authorized');
        this.code = 401;
    }
}

class SessionExpiredError extends Error {
    constructor(message) {
        super(message || 'Session invalid');
        this.code = 401;
    }
}

class InvalidCredentialsError extends Error {
    constructor(message) {
        super(message || 'Invalid user and/or password');
        this.code = 401;
    }
}

class NotImplementedError extends Error {
    constructor(message) {
        super(message || 'Method not implemented');
        this.code = 501;
    }
}

module.exports = {
    UnauthorizedError,
    UserNotFoundError,
    BadRequestError,
    SessionExpiredError,
    InvalidCredentialsError,
    NotImplementedError
};