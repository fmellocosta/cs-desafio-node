const MONGO_SERVER = process.env.MONGO_SERVER || 'localhost';
const MONGO_PORT = process.env.MONGO_PORT || 27017;

const mongoose = require('mongoose');
const winston = require('winston');

let uri = `mongodb://${MONGO_SERVER}:${MONGO_PORT}/cs`;

function init() {
    winston.info(`Connecting to ${uri}`);
    mongoose.connect(uri);
    mongoose.connection;
}

module.exports = { init };