const JWT_SECRET = process.env.JWT_SECRET || 'secret';
const JWT_EXPIRE = process.env.JWT_EXPIRE || 30;

const jwt = require('jsonwebtoken');

function generateToken(data) {
    return jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (JWT_EXPIRE * 60),
        data: data
    }, JWT_SECRET);
}

function validateToken(token) {
    return jwt.verify(token, JWT_SECRET, function(error, decoded){
        if (error)
            return false;
        return decoded;
    });
}

function retrieveToken(headers) {
    if (!headers && !headers['authorization'])
        return null;
        
    let match = /Bearer (.*)/.exec(headers['authorization']);
    if (!match)
        return null;

    return match[1];
}

module.exports = { retrieveToken, generateToken, validateToken };