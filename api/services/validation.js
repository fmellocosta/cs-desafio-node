function validateSignupFields(fields) {

    if  (fields.constructor === Object && Object.keys(fields).length === 0)
        return false;

    if (!fields.nome || !fields.email || !fields.senha || !fields.telefone.area || !fields.telefone.numero)
        return false;

    return true;
    
}

function validateSigninFields(fields) {

    if  (fields.constructor === Object && Object.keys(fields).length === 0)
        return false;

    if (!fields.email || !fields.senha)
        return false;

    return true;
    
}

module.exports = { validateSignupFields, validateSigninFields };