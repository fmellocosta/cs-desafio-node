const express = require('express');
const winston = require('winston');
const mongoose = require('mongoose');

let User = require('../models/user');
let Errors = require('../services/handle_errors');

const { retrieveToken, generateToken, validateToken } = require('../services/authentication');
const { validateSignupFields, validateSigninFields } = require('../services/validation');
const { handle } = require('../services/handle_response');

const router = new express.Router();

router.get('/user/:id', (req, res) => {
    
    try {
        
        let authToken = retrieveToken(req.headers);
        if (!authToken) {
            winston.debug('Request without auth token');
            throw new Errors.UnauthorizedError();
        }
        
        var id = req.params.id;

        User.findById(id, function (error, result) {
            try {
                
                if (!result) {
                    winston.debug(`Id [${id}] not found in database`);
                    throw new Errors.UserNotFoundError();
                }

                if (result.token != authToken) {
                    winston.debug('Token not equal to requested model');
                    throw new Errors.UnauthorizedError();
                }

                if (!validateToken(result.token)) {
                    winston.debug('Token expired');
                    throw new Errors.SessionExpiredError();
                }

                winston.debug(`Retrieving data for id ${id}`);
                handle(res, 200, result);

            } catch (error) {
                handle(res, error.code, error.message);
            }
        });

    } catch (error) {
        handle(res, error.code, error.message);
    }

});

router.post('/user/signup', (req, res) => {

    try {

        if (!validateSignupFields(req.body)) {
            winston.debug('Missing some fields in request');
            throw new Errors.BadRequestError();
        }

        let data = {
            name: req.body.nome,
            email: req.body.email,
            password: req.body.senha,
            phone_area: req.body.telefone.area,
            phone_number: req.body.telefone.numero
        };

        winston.debug(`Checking if email [${data.email}] exists`);
        User.checkEmail(data)
            .then(function (result) {
                if (result) {
                    winston.debug(`Email [${data.email}] already exists in database`);
                    handle(res, 400, 'Email already exists');
                } else {
                    winston.debug(`Inserting user [${JSON.stringify(data)}]`);
                    User.saveWithToken(data)
                        .then(function (result) {
                            handle(res, 200, result);
                        });
                }
            });

    } catch (error) {
        handle(res, error.code, error.message);
    }

});

router.post('/user/signin', (req, res) => {

    try {

        if (!validateSigninFields(req.body)) {
            winston.debug('Missing some fields in request');
            throw new Errors.BadRequestError();
        }

        let data = {
            email: req.body.email,
            password: req.body.senha
        };

        winston.debug('Checking if combination user/password exists');
        User.findOne({ 'email': data.email })
            .then((user) => {

                if (!user) {
                    winston.debug(`Email [${data.email}] not found`);
                    return Promise.reject(new Errors.InvalidCredentialsError());
                }
                
                user.verifyPassword(data.password)
                    .then(function (valid) {
                        if (!valid) { 
                            winston.debug('Password not matched');
                            return Promise.reject(new Errors.InvalidCredentialsError());
                        }
                        return Promise.resolve(user);
                    })
                    .then((user) => {
                        winston.debug(`Signing in user [${user.id}]`);
                        user.dt_last_login = Date.now();
                        user.token = generateToken(user.email);
                        user.save();
                        res.status(200).json(user);
                    })                    
                    .catch(function(error){
                        handle(res, error.code, error.message);
                    });
            })
            .catch(function(error){
                handle(res, error.code, error.message);
            });

    } catch (error) {
        handle(res, error.code, error.message);
    }

});

module.exports = router;