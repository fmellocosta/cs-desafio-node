const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');

let Schema = mongoose.Schema;

const { generateToken } = require('../services/authentication');

let userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, bcrypt: true, required: true },
    phone_area: { type: String, required: true, length: 2 },
    phone_number: { type: String, required: true, length: 9 },
    dt_create: { type: Date },
    dt_update: { type: Date },
    dt_last_login: { type: Date },
    token: { type: String }
}, { versionKey: false }
);

userSchema.plugin(bcrypt);

userSchema.static('checkEmail', function (data, callback) {
    return this.model('User')
        .count({ 'email': data.email }, function (error, count) {
            return count > 0;
        })
        .then(callback);
});

userSchema.static('saveWithToken', function (data, callback) {
    return this.model('User')
        .create(data)
        .then(user => {
            let currentDate = new Date().toISOString(); 
            user.dt_create = currentDate;
            user.dt_last_login = currentDate;
            user.token = generateToken(user.email);
            return user.save();
        })
        .then(callback);
});

module.exports = mongoose.model('User', userSchema);